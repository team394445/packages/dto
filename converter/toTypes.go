package converter

import "go/ast"

func fieldTypeToString(fieldType ast.Expr, structs map[string]bool) string {
	switch fieldType := fieldType.(type) {
	case *ast.Ident:

		if structs[fieldType.Name] {
			return fieldType.Name
		}
		return typeToTS(fieldType.Name)
	case *ast.SelectorExpr:
		if fieldType.Sel.Name == "UUID" {
			return "string"
		}
		if fieldType.Sel.Name == "Time" {
			return "number"
		}

		return fieldType.Sel.Name
	case *ast.ArrayType:
		return "Array<" + fieldTypeToString(fieldType.Elt, structs) + ">"
	case *ast.StarExpr:
		return fieldTypeToString(fieldType.X, structs)
	default:
		return "any"
	}
}

func typeToTS(goType string) string {

	switch goType {
	case "int", "int8", "int16", "int32", "int64", "uint", "uint8", "uint16", "uint32", "uint64":
		return "number"
	case "float32", "float64":
		return "number"
	case "string", "uuid":
		return "string"
	case "bool":
		return "boolean"
	default:
		return "any"
	}
}
