package converter

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"os"
	"path/filepath"
	"reflect"
	"strings"
)

func ToInterface(dir string, outputDir string) error {

	files, err := GetFilesAndFolders(dir)
	if err != nil {
		fmt.Println(err)
		return err
	}
	for _, file := range files {
		fileContent, err := ReadFile(file)
		if err != nil {
			fmt.Println(err)
			continue
		}
		interfaces, comments, interfaceNameList := convertToInterfaces(fileContent)

		pkgName := getPackageName(fileContent)
		outputSubDir := filepath.Join(outputDir, pkgName)
		if err := os.MkdirAll(outputSubDir, os.ModePerm); err != nil {
			fmt.Println("Error creating directory:", err)
			continue
		}
		modelFileName := strings.TrimSuffix(filepath.Base(file), ".go")
		outputInterfaceFile := filepath.Join(outputSubDir, modelFileName)
		outputServiceFile := filepath.Join(outputSubDir, strings.TrimSuffix(filepath.Base(file), ".go")+"Service")

		var services []string
		for _, commentGroup := range comments {
			for _, comment := range commentGroup.List {
				if strings.HasPrefix(comment.Text, "//api") {

					parts := strings.Split(comment.Text, ":")
					method := parts[1]
					endPoint := parts[2]
					funcName := parts[3]
					reqModel := parts[4]
					resModel := parts[5]

					service := toService(funcName, method, endPoint, reqModel, resModel)

					services = append(services, service)
					fmt.Println(service)
				}
			}
		}
		globalImport := "import * as global from '../global'\n"

		structImports := strings.Join(interfaceNameList, ",")

		interfaceImports := fmt.Sprintf("import {%s} from './%s'\n", structImports, modelFileName)

		interfaceStr := strings.Join(interfaces, "\n")
		serviceStr := strings.Join(services, "\n")

		if err := WriteToFile(outputInterfaceFile+".ts", interfaceStr+"\n"); err != nil {
			fmt.Println(err)
			continue
		}
		if err := WriteToFile(outputServiceFile+".ts", globalImport+"\n"+interfaceImports+"\n"+serviceStr+"\n"); err != nil {
			fmt.Println(err)
			continue
		}

	}
	if err := WriteToFile(outputDir+"/index.ts", "\n\n"); err != nil {
		fmt.Println(err)

	}
	generateIndex(outputDir)
	return nil
}

func toService(funcName string, method string, endPoint string, reqModel string, resModel string) string {
	if method == "post" {
		return fmt.Sprintf("export async function %s(model: %s) {\n"+
			"  const fetcher = new global.Fetcher(global.getEndPoint());\n"+
			"  const response = await fetcher.%s<\n"+
			"    %s,\n"+
			"    %s\n"+
			"  >(\"%s\", model);\n"+
			"  return response;\n"+
			"}", funcName, reqModel, method, reqModel, resModel, endPoint)
	}
	if method == "get" {
		if reqModel == "-" {
			return fmt.Sprintf("export async function %s() {\n"+
				"  const fetcher = new global.Fetcher(global.getEndPoint());\n"+
				"  const response = await fetcher.%s<\n"+
				"    %s\n"+
				"  >(\"%s\");\n"+
				"  return response;\n"+
				"}", funcName, method, resModel, endPoint)
		} else {
			return fmt.Sprintf("export async function %s(requestId?: string) {\n"+
				"  const fetcher = new global.Fetcher(global.getEndPoint());\n"+
				"  const response = await fetcher.%s<\n"+
				"    %s\n"+
				"  >(\"%s\" + '/' + requestId);\n"+
				"  return response;\n"+
				"}", funcName, method, resModel, endPoint)
		}
	}
	if method == "put" {
		return fmt.Sprintf("export async function %s(model: %s) {\n"+
			"  const fetcher = new global.Fetcher(global.getEndPoint());\n"+
			"  const response = await fetcher.%s<\n"+
			"    %s,\n"+
			"    %s\n"+
			"  >(\"%s\", model);\n"+
			"  return response;\n"+
			"}", funcName, reqModel, method, reqModel, resModel, endPoint)
	}

	return ""

}

func getPackageName(src string) string {
	fset := token.NewFileSet()
	node, err := parser.ParseFile(fset, "src.go", src, parser.PackageClauseOnly)
	if err != nil {
		fmt.Println("Error parsing Go source:", err)
		return ""
	}
	return node.Name.Name
}
func convertToInterfaces(src string) ([]string, []*ast.CommentGroup, []string) {
	fset := token.NewFileSet()
	node, err := parser.ParseFile(fset, "src.go", src, parser.ParseComments)
	if err != nil {
		fmt.Println("Error parsing Go source:", err)
		return nil, nil, nil
	}

	var interfaces []string
	var interfaceNameList []string
	var comments []*ast.CommentGroup
	structs := make(map[string]bool)

	for _, comment := range node.Comments {
		comments = append(comments, comment)
	}

	for _, decl := range node.Decls {
		if genDecl, ok := decl.(*ast.GenDecl); ok && genDecl.Tok == token.TYPE {
			for _, spec := range genDecl.Specs {
				typeSpec, ok := spec.(*ast.TypeSpec)
				if !ok {
					continue
				}
				structName := typeSpec.Name.Name
				interfaceNameList = append(interfaceNameList, structName)
				structs[structName] = true
				structType, ok := typeSpec.Type.(*ast.StructType)
				if !ok {
					continue
				}

				interfaceStr := fmt.Sprintf("export interface %s {\n", structName)
				for _, field := range structType.Fields.List {
					fieldType := fieldTypeToString(field.Type, structs)
					for _, name := range field.Names {
						optional := false
						if field.Tag != nil {
							tag := reflect.StructTag(field.Tag.Value[1 : len(field.Tag.Value)-1])
							tagValue, _ := tag.Lookup("json")
							if tagValue != "" {
								opts := strings.Split(tagValue, ",")
								for _, opt := range opts {
									if opt == "omitempty" {
										optional = true
										break
									}
								}
							}

						}
						if optional {
							interfaceStr += fmt.Sprintf("    %s?: %s;\n", toLowerFirst(name.Name), fieldType)
						} else {
							interfaceStr += fmt.Sprintf("    %s: %s;\n", toLowerFirst(name.Name), fieldType)
						}

					}
				}
				interfaceStr += "}"
				interfaces = append(interfaces, interfaceStr)
			}
		}
	}
	return interfaces, comments, interfaceNameList
}

func generateIndex(directory string) {

	// Open or create the index.ts file
	indexFile, err := os.Create(filepath.Join(directory, "index.ts"))
	if err != nil {
		fmt.Println("Error creating index.ts:", err)
		return
	}
	defer indexFile.Close()

	// Write the imports for all TypeScript files in the subdirectories
	err = filepath.Walk(directory, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() && info.Name() == "global" {
			return filepath.SkipDir
		}

		if !info.IsDir() && strings.HasSuffix(info.Name(), ".ts") {
			// Get the relative path of the TypeScript file
			relPath, err := filepath.Rel(directory, path)

			if err != nil {
				return err
			}
			fmt.Println(relPath)
			if relPath != "index.ts" {

				// Write the import statement to the index.ts file
				pathName := strings.TrimSuffix(relPath, ".ts")
				_, err = fmt.Fprintf(indexFile, "export * from './%s';\n", pathName)
				if err != nil {
					return err
				}
			}
		}
		return nil
	})
	if err != nil {
		fmt.Println("Error walking directory:", err)
		return
	}

	fmt.Println("index.ts generated successfully!")
}
