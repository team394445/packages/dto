package converter

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"unicode"
)

func WriteToFile(filename, content string) error {
	return ioutil.WriteFile(filename, []byte(content), 0644)
}
func ReadFile(filePath string) (string, error) {

	// Open the file
	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println("Error:", err)
		return "", err
	}
	defer file.Close()

	// Read the contents of the file
	content, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println("Error:", err)
		return "", err
	}

	return string(content), nil
}

func GetFilesAndFolders(dir string) ([]string, error) {
	var files []string

	// Walk through the directory recursively
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		// Exclude the root directory itself
		if !info.IsDir() { // Check if it's a file
			files = append(files, path)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	return files, nil
}

func removeLeadingHyphen(s string) string {
	// Check if the string starts with a hyphen
	if len(s) > 0 && s[0] == '-' {
		// Remove the hyphen using string slicing
		return s[1:]
	}
	return s
}
func startsWithDash(s string) bool {
	if len(s) == 0 {
		return false
	}
	return s[0] == '-'
}

func toLowerFirst(s string) string {
	if len(s) == 0 {
		return s
	}
	runes := []rune(s)
	runes[0] = unicode.ToLower(runes[0])
	return string(runes)
}
