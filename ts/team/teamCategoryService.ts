import * as global from '../global'

import {TeamCategory,ListTeamCategoryResponse} from './teamCategory'

export async function GetTeamCategory() {
  const fetcher = new global.Fetcher(global.getEndPoint());
  const response = await fetcher.get<
    ListTeamCategoryResponse
  >("team/category");
  return response;
}
