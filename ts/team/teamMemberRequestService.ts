import * as global from '../global'

import {OwnTeamMemberRequest,TeamMemberRequest,OwnTeamMemberResponse,ListTeamMemberRequest,TeamMemberListRequest,RejectJoinToTeamRequest,RequestForJoinToTeamRequest,UpdateRequestTeamRequest,RequestForJoinToTeamResponse} from './teamMemberRequest'

export async function GetOwnTeamMemberRequest() {
  const fetcher = new global.Fetcher(global.getEndPoint());
  const response = await fetcher.get<
    OwnTeamMemberResponse
  >("team/request/getOwnTeamMemberRequest");
  return response;
}
export async function GetTeamMemberRequestList(requestId?: string) {
  const fetcher = new global.Fetcher(global.getEndPoint());
  const response = await fetcher.get<
    ListTeamMemberRequest
  >("team/request/getByTeamId" + '/' + requestId);
  return response;
}
export async function RequestForJoinToTeam(model: RequestForJoinToTeamRequest) {
  const fetcher = new global.Fetcher(global.getEndPoint());
  const response = await fetcher.post<
    RequestForJoinToTeamRequest,
    RequestForJoinToTeamResponse
  >("team/request/requestForJoinToTeam", model);
  return response;
}
export async function RejectJoinRequest(requestId?: string) {
  const fetcher = new global.Fetcher(global.getEndPoint());
  const response = await fetcher.get<
    string
  >("team/request/rejectJoinRequest" + '/' + requestId);
  return response;
}
