export interface TeamCategory {
    id: string;
    name: string;
    title: string;
    createdAt: number;
    updatedAt: number;
}
export interface ListTeamCategoryResponse {
    teamCategoryList: Array<TeamCategory>;
}
