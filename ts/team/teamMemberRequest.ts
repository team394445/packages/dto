export interface OwnTeamMemberRequest {
    id: string;
    teamId: string;
    userId: string;
    name: string;
    firstName: string;
    lastName: string;
    createAt: number;
    updatedAt: number;
}
export interface TeamMemberRequest {
    id: string;
    teamId: string;
    userId: string;
    status: string;
    createAt: number;
    updatedAt: number;
}
export interface OwnTeamMemberResponse {
    members: Array<OwnTeamMemberRequest>;
}
export interface ListTeamMemberRequest {
    members: Array<TeamMemberRequest>;
}
export interface TeamMemberListRequest {
    request: TeamMemberRequest;
}
export interface RejectJoinToTeamRequest {
    teamId: string;
    userId: string;
}
export interface RequestForJoinToTeamRequest {
    teamId: string;
}
export interface UpdateRequestTeamRequest {
    status: string;
}
export interface RequestForJoinToTeamResponse {
    teamId: string;
}
