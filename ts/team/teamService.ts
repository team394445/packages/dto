import * as global from '../global'

import {Team,ListTeamResponse,GetTeamByIdRequest,GetTeamByIdResponse,CreateTeamRequest,CreateTeamResponse,UpdateTeamRequest,UpdateTeamResponse} from './team'

export async function CreateTeam(model: CreateTeamRequest) {
  const fetcher = new global.Fetcher(global.getEndPoint());
  const response = await fetcher.post<
    CreateTeamRequest,
    CreateTeamResponse
  >("team/createTeam", model);
  return response;
}
export async function ListTeam() {
  const fetcher = new global.Fetcher(global.getEndPoint());
  const response = await fetcher.get<
    ListTeamResponse
  >("team/listTeam");
  return response;
}
export async function GetMyTeams(requestId?: string) {
  const fetcher = new global.Fetcher(global.getEndPoint());
  const response = await fetcher.get<
    ListTeamResponse
  >("team/myTeams" + '/' + requestId);
  return response;
}
export async function GetTeamById(requestId?: string) {
  const fetcher = new global.Fetcher(global.getEndPoint());
  const response = await fetcher.get<
    GetTeamByIdResponse
  >("team" + '/' + requestId);
  return response;
}
export async function UpdateTeam(model: UpdateTeamRequest) {
  const fetcher = new global.Fetcher(global.getEndPoint());
  const response = await fetcher.put<
    UpdateTeamRequest,
    boolean
  >("team", model);
  return response;
}
