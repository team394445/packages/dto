import * as global from '../global'

import {Post,GetPostsByTeamIdResponse,CreatePostRequest,CreatePostResponse} from './post'

export async function GetPostsByTeamId(requestId?: string) {
  const fetcher = new global.Fetcher(global.getEndPoint());
  const response = await fetcher.get<
    GetPostsByTeamIdResponse
  >("team/post/list" + '/' + requestId);
  return response;
}
export async function CreatePost(model: CreatePostRequest) {
  const fetcher = new global.Fetcher(global.getEndPoint());
  const response = await fetcher.post<
    CreatePostRequest,
    CreatePostResponse
  >("team/post", model);
  return response;
}
