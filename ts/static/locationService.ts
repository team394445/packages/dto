import * as global from '../global'

import {City,Province,Country,GetCountryListRequest,GetCountryListResponse,GetProvinceByCountryIdRequest,GetProvinceByCountryIdResponse,GetCityByProvinceIdRequest,GetCityByProvinceIdResponse} from './location'

export async function GetCountryList() {
  const fetcher = new global.Fetcher(global.getEndPoint());
  const response = await fetcher.get<
    GetCountryListResponse
  >("static/GetCountryList");
  return response;
}
export async function GetProvinceList(requestId?: string) {
  const fetcher = new global.Fetcher(global.getEndPoint());
  const response = await fetcher.get<
    GetProvinceByCountryIdResponse
  >("static/GetProvinceList" + '/' + requestId);
  return response;
}
export async function GetCityList(requestId?: string) {
  const fetcher = new global.Fetcher(global.getEndPoint());
  const response = await fetcher.get<
    GetCityByProvinceIdResponse
  >("static/GetCityList" + '/' + requestId);
  return response;
}
