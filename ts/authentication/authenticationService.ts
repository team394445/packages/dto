import * as global from '../global'

import {SendVerificationCodeRequest,SendVerificationCodeResponse,SignInRequest,SignInResponse,CompleteRegisterRequest,CompleteRegisterResponse} from './authentication'

export async function SendVerificationCode(model: SendVerificationCodeRequest) {
  const fetcher = new global.Fetcher(global.getEndPoint());
  const response = await fetcher.post<
    SendVerificationCodeRequest,
    SendVerificationCodeResponse
  >("auth/SendVerificationCode", model);
  return response;
}
export async function SignIn(model: SignInRequest) {
  const fetcher = new global.Fetcher(global.getEndPoint());
  const response = await fetcher.post<
    SignInRequest,
    SignInResponse
  >("auth/SignIn", model);
  return response;
}
export async function CompleteRegister(model: CompleteRegisterRequest) {
  const fetcher = new global.Fetcher(global.getEndPoint());
  const response = await fetcher.post<
    CompleteRegisterRequest,
    CompleteRegisterResponse
  >("auth/CompleteRegister", model);
  return response;
}
