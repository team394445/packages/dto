export interface SendVerificationCodeRequest {
    phoneNumber: string;
}
export interface SendVerificationCodeResponse {
    token: string;
}
export interface SignInRequest {
    phoneNumber: string;
    code: string;
}
export interface SignInResponse {
    refreshToken: string;
    token: string;
    firstName: string;
    lastName: string;
    avatarFileId: string;
    isFirstLogin: boolean;
}
export interface CompleteRegisterRequest {
    token: string;
    phoneNumber: string;
    firstName: string;
    lastName: string;
    avatarFileId: string;
}
export interface CompleteRegisterResponse {
    userId: string;
    token: string;
}
