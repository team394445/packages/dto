enum METHOD {
  POST = "POST",
  GET = "GET",
  PUT = "PUT",
  DELETE = "DELETE",
}

function getToken() {
  const token = localStorage.getItem("token");
  if (token) {
    return token;
  }
  return undefined;
}

function getOption(method: string, body?: any, isFile = false): any {
  const options: any = {
    method: method || METHOD.GET,
    headers: {
      "Content-Type": "application/json",
    },
  };
  const token = getToken();
  console.log("token", token);
  if (isFile) {
    delete options.headers["Content-Type"];
  }
  if (body) {
    options.body = !isFile ? JSON.stringify(body) : body;
  }
  if (token) {
    options.headers["Authorization"] = token;
  }

  return options;
}

export class Fetcher {
  private baseUrl: string;
  constructor(baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  async get<T>(endpoint: string): Promise<ResponseModel<T>> {
    try {
      const url = this.baseUrl + endpoint;
      const options = getOption(METHOD.GET);
      const res = await fetch(url, options);

      return toResponse<T>(res);
    } catch (error) {
      return toCatchError<T>(error);
    }
  }

  async post<T, U>(endpoint: string, body: T): Promise<ResponseModel<U>> {
    try {
      const url = this.baseUrl + endpoint;
      const options = getOption(METHOD.POST, body);
      const res = await fetch(url, options);
      return toResponse<U>(res);
    } catch (error) {
      return toCatchError<U>(error);
    }
  }
  async put<T, U>(endpoint: string, body: T): Promise<ResponseModel<U>> {
    try {
      const url = this.baseUrl + endpoint;
      const options = getOption(METHOD.PUT, body);
      const res = await fetch(url, options);
      return toResponse<U>(res);
    } catch (error) {
      return toCatchError<U>(error);
    }
  }

  // async getFile(url: string): Promise<any> {
  //   const config = getConfiguration();
  //   const options = getOption(METHOD.GET);
  //   const res = await fetch(config.apiEndpoint + url, options);
  //   const data = await res.blob();
  //   return data;
  // }
  // post

  // postForm
  // async postForm(url: string, body: any): Promise<ResponseModel<T>> {
  //   const config = getConfiguration();
  //   const options = getOption(METHOD.POST, body, true);
  //   const res = await fetch(config.apiEndpoint + url, options);
  //   const data: ResponseModel<T> = await res.json();
  //   return data;
  // }

  // putForm
  // async putForm(url, body) {
  // 	const options = {
  // 		method: 'PUT',
  // 		body
  // 	};
  // 	const res = await fetch(this.endpoint + url, options);
  // 	const data = await res.json();
  // 	return data;
  // }

  // delete
  // async delete(url: string, body: any): Promise<ResponseModel<T>> {
  //   const config = getConfiguration();
  //   const options = getOption(METHOD.DELETE, body);
  //   const res = await fetch(config.apiEndpoint + url, options);
  //   const data: ResponseModel<T> = await res.json();
  //   return data;
  // }

  // deleteForm
  // async deleteForm(url:string, body:any) {
  // 	const options = {
  // 		method: 'DELETE',
  // 		body
  // 	};
  // 	const res = await fetch(this.endpoint + url, options);
  // 	const data = await res.json();
  // 	return data;
  // }
}

export async function toResponse<T>(res: globalThis.Response) {
  if (res.ok) {
    const responseBody: ResponseModel<T> = {
      data: (await res.json()) as T,
      status: true,
      statusCode: 200,
    };
    return responseBody;
  } else {
    const errorBody: ResponseModel<T> = (await res.json()) as ResponseModel<T>;
    const responseBody: ResponseModel<T> = {
      status: false,
      statusCode: res.status,
      fields: errorBody.fields,
      message: errorBody.message,
    };
    return responseBody;
  }
}

export async function toCatchError<T>(err: any): Promise<ResponseModel<T>> {
  return {
    status: false,
    statusCode: 500,
    fields: undefined,
    message: err.message,
  };
}
export interface ResponseModel<T> {
  data?: T;
  status: boolean;
  statusCode: number;
  message?: string;
  fields?: { [key: string]: string };
}
