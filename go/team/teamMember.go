package team

import "github.com/google/uuid"

type AddMemberToTeamRequest struct {
	TeamId uuid.UUID `json:"team_id"`
	UserId uuid.UUID `json:"user_id"`
	Role   string    `json:"role"`
}

type AddMemberToTeamResponse struct {
	TeamId uuid.UUID `json:"team_id"`
	UserId uuid.UUID `json:"user_id"`
	Role   string    `json:"role"`
}
