package team

// method:endpoint:funcName:ReqModel:resModel

//api:get:team/post/list:GetPostsByTeamId:string:GetPostsByTeamIdResponse
//api:post:team/post:CreatePost:CreatePostRequest:CreatePostResponse

import (
	"time"

	"github.com/google/uuid"
)

type Post struct {
	Id           uuid.UUID  `json:"id"`
	TeamId       uuid.UUID  `json:"teamId"`
	Title        string     `json:"title"`
	Description  string     `json:"description"`
	IsPublic     bool       `json:"is_public"`
	LikeCount    int        `json:"like_count"`
	DislikeCount int        `json:"dislike_count"`
	ViewCount    int        `json:"view_count"`
	AllowComment bool       `json:"allow_comment"`
	IsPublished  bool       `json:"is_published"`
	CreatorId    uuid.UUID  `json:"creator_id"`
	CreateAt     *time.Time `json:"create_at"`
	UpdateAt     *time.Time `json:"update_at"`
}

type GetPostsByTeamIdResponse struct {
	PostList []Post `json:"postList"`
}

type CreatePostRequest struct {
	Post Post `json:"post"`
}
type CreatePostResponse struct {
	PostId string `json:"postId"`
}
