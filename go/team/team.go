package team

// method:endpoint:funcName:ReqModel:resModel

//api:post:team/createTeam:CreateTeam:CreateTeamRequest:CreateTeamResponse
//api:get:team/listTeam:ListTeam:-:ListTeamResponse
//api:get:team/myTeams:GetMyTeams:string:ListTeamResponse
//api:get:team:GetTeamById:string:GetTeamByIdResponse
//api:put:team:UpdateTeam:UpdateTeamRequest:boolean

import (
	"github.com/google/uuid"
)

type Team struct {
	Id          uuid.UUID `json:"id,omitempty"`
	Name        string    `json:"name"`
	Title       string    `json:"title"`
	IsPublic    bool      `json:"isPublic"`
	Description string    `json:"description"`
	CountryId   uuid.UUID `json:"countryId,omitempty"`
	ProvinceId  uuid.UUID `json:"provinceId,omitempty"`
	CityId      uuid.UUID `json:"cityId,omitempty"`
	CategoryId  uuid.UUID `json:"categoryId,omitempty"`
}

type ListTeamResponse struct {
	Teams []Team `json:"teams"`
}

type GetTeamByIdRequest struct {
	TeamId string
}
type GetTeamByIdResponse struct {
	Team Team `json:"team"`
}

type CreateTeamRequest struct {
	Team Team `json:"team"`
}
type CreateTeamResponse struct {
	TeamId string `json:"team_id"`
}

type UpdateTeamRequest struct {
	Team Team `json:"team"`
}
type UpdateTeamResponse struct {
	Team Team `json:"team"`
}
