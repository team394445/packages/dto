package team

// method:endpoint:funcName:ReqModel:resModel

//api:get:team/request/getOwnTeamMemberRequest:GetOwnTeamMemberRequest:-:OwnTeamMemberResponse
//api:get:team/request/getByTeamId:GetTeamMemberRequestList:string:ListTeamMemberRequest
//api:post:team/request/requestForJoinToTeam:RequestForJoinToTeam:RequestForJoinToTeamRequest:RequestForJoinToTeamResponse
//api:get:team/request/rejectJoinRequest:RejectJoinRequest:string:string

import (
	"time"

	"github.com/google/uuid"
)

type OwnTeamMemberRequest struct {
	Id        uuid.UUID  `json:"id"`
	TeamId    uuid.UUID  `json:"teamId"`
	UserId    uuid.UUID  `json:"userId"`
	Name      string     `json:"name"`
	FirstName string     `json:"firstName"`
	LastName  string     `json:"lastName"`
	CreateAt  *time.Time `json:"createAt"`
	UpdatedAt *time.Time `json:"updatedAt"`
}
type TeamMemberRequest struct {
	Id        uuid.UUID  `json:"id"`
	TeamId    uuid.UUID  `json:"teamId"`
	UserId    uuid.UUID  `json:"userId"`
	Status    string     `json:"status"`
	CreateAt  *time.Time `json:"createAt"`
	UpdatedAt *time.Time `json:"updatedAt"`
}

type OwnTeamMemberResponse struct {
	Members []OwnTeamMemberRequest `json:"members"`
}

type ListTeamMemberRequest struct {
	Members []TeamMemberRequest `json:"members"`
}

type TeamMemberListRequest struct {
	Request TeamMemberRequest `json:"members"`
}

type RejectJoinToTeamRequest struct {
	TeamId string `json:"teamId"`
	UserId string `json:"userId"`
}
type RequestForJoinToTeamRequest struct {
	TeamId string `json:"teamId"`
}

type UpdateRequestTeamRequest struct {
	Status string `json:"status"`
}

type RequestForJoinToTeamResponse struct {
	TeamId string `json:"teamId"`
}
