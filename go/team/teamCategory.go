package team

//api:get:team/category:GetTeamCategory:-:ListTeamCategoryResponse
import (
	"time"

	"github.com/google/uuid"
)

type TeamCategory struct {
	Id        uuid.UUID  `json:"id"`
	Name      string     `json:"name"`
	Title     string     `json:"title"`
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
}
type ListTeamCategoryResponse struct {
	TeamCategoryList []TeamCategory `json:"teamCategory"`
}
