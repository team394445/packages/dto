package static

// method:endpoint:funcName:ReqModel:resModel
//api:get:static/GetCountryList:GetCountryList:-:GetCountryListResponse
//api:get:static/GetProvinceList:GetProvinceList:string:GetProvinceByCountryIdResponse
//api:get:static/GetCityList:GetCityList:string:GetCityByProvinceIdResponse

import (
	"github.com/google/uuid"
)

type City struct {
	Id         uuid.UUID `json:"id"`
	ProvinceId uuid.UUID `json:"province_id"`
	Name       string    `json:"name"`
}

type Province struct {
	Id        uuid.UUID `json:"id"`
	CountryId uuid.UUID `json:"country_id"`
	Name      string    `json:"name"`
}

type Country struct {
	Id   uuid.UUID `json:"id"`
	Name string    `json:"name"`
}

type GetCountryListRequest struct {
}
type GetCountryListResponse struct {
	CountryList []Country `json:"countryList"`
}

type GetProvinceByCountryIdRequest struct {
	CountryId uuid.UUID `json:"countryId"`
}
type GetProvinceByCountryIdResponse struct {
	ProvinceList []Province `json:"provinceList"`
}

type GetCityByProvinceIdRequest struct {
	ProvinceId uuid.UUID `json:"provinceId"`
}
type GetCityByProvinceIdResponse struct {
	CityList []City `json:"cityList"`
}
