package authentication

// method:endpoint:funcName:ReqModel:resModel

//api:post:auth/SendVerificationCode:SendVerificationCode:SendVerificationCodeRequest:SendVerificationCodeResponse
//api:post:auth/SignIn:SignIn:SignInRequest:SignInResponse
//api:post:auth/CompleteRegister:CompleteRegister:CompleteRegisterRequest:CompleteRegisterResponse

type SendVerificationCodeRequest struct {
	PhoneNumber string `json:"phoneNumber"`
}
type SendVerificationCodeResponse struct {
	Token string `json:"token"`
}

//-------------------------------------------

type SignInRequest struct {
	PhoneNumber string `json:"phoneNumber"`
	Code        string `json:"code"`
}
type SignInResponse struct {
	RefreshToken string `json:"refreshToken"`
	Token        string `json:"token"`
	FirstName    string `json:"firstName"`
	LastName     string `json:"lastName"`
	AvatarFileId string `json:"avatarFileId"`
	IsFirstLogin bool   `json:"isFirstLogin"`
}

//-------------------------------------------

type CompleteRegisterRequest struct {
	Token        string `json:"token"`
	PhoneNumber  string `json:"phoneNumber"`
	FirstName    string `json:"firstName"`
	LastName     string `json:"lastName"`
	AvatarFileId string `json:"avatarFileId"`
}

type CompleteRegisterResponse struct {
	UserId string `json:"userId"`
	Token  string `json:"token"`
}
