package main

import (
	"fmt"
	"os"

	converter "gitlab.com/team394445/packages/api/converter"
)

func main() {
	cwd, err := os.Getwd()
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	converter.ToInterface(cwd+"/go", cwd+"/ts")

}
