export declare class Fetcher {
    private baseUrl;
    constructor(baseUrl: string);
    get<T>(endpoint: string): Promise<ResponseModel<T>>;
    post<T, U>(endpoint: string, body: T): Promise<ResponseModel<U>>;
    put<T, U>(endpoint: string, body: T): Promise<ResponseModel<U>>;
}
export declare function toResponse<T>(res: globalThis.Response): Promise<ResponseModel<T>>;
export declare function toCatchError<T>(err: any): Promise<ResponseModel<T>>;
export interface ResponseModel<T> {
    data?: T;
    status: boolean;
    statusCode: number;
    message?: string;
    fields?: {
        [key: string]: string;
    };
}
