var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);

// ts/index.ts
var ts_exports = {};
__export(ts_exports, {
  CompleteRegister: () => CompleteRegister,
  CreatePost: () => CreatePost,
  CreateTeam: () => CreateTeam,
  GetCityList: () => GetCityList,
  GetCountryList: () => GetCountryList,
  GetMyTeams: () => GetMyTeams,
  GetOwnTeamMemberRequest: () => GetOwnTeamMemberRequest,
  GetPostsByTeamId: () => GetPostsByTeamId,
  GetProvinceList: () => GetProvinceList,
  GetTeamById: () => GetTeamById,
  GetTeamCategory: () => GetTeamCategory,
  GetTeamMemberRequestList: () => GetTeamMemberRequestList,
  ListTeam: () => ListTeam,
  RejectJoinRequest: () => RejectJoinRequest,
  RequestForJoinToTeam: () => RequestForJoinToTeam,
  SendVerificationCode: () => SendVerificationCode,
  SignIn: () => SignIn,
  UpdateTeam: () => UpdateTeam
});
module.exports = __toCommonJS(ts_exports);

// ts/global/fetcher.ts
function getToken() {
  const token = localStorage.getItem("token");
  if (token) {
    return token;
  }
  return void 0;
}
function getOption(method, body, isFile = false) {
  const options = {
    method: method || "GET" /* GET */,
    headers: {
      "Content-Type": "application/json"
    }
  };
  const token = getToken();
  console.log("token", token);
  if (isFile) {
    delete options.headers["Content-Type"];
  }
  if (body) {
    options.body = !isFile ? JSON.stringify(body) : body;
  }
  if (token) {
    options.headers["Authorization"] = token;
  }
  return options;
}
var Fetcher = class {
  constructor(baseUrl) {
    this.baseUrl = baseUrl;
  }
  async get(endpoint) {
    try {
      const url = this.baseUrl + endpoint;
      const options = getOption("GET" /* GET */);
      const res = await fetch(url, options);
      return toResponse(res);
    } catch (error) {
      return toCatchError(error);
    }
  }
  async post(endpoint, body) {
    try {
      const url = this.baseUrl + endpoint;
      const options = getOption("POST" /* POST */, body);
      const res = await fetch(url, options);
      return toResponse(res);
    } catch (error) {
      return toCatchError(error);
    }
  }
  async put(endpoint, body) {
    try {
      const url = this.baseUrl + endpoint;
      const options = getOption("PUT" /* PUT */, body);
      const res = await fetch(url, options);
      return toResponse(res);
    } catch (error) {
      return toCatchError(error);
    }
  }
  // async getFile(url: string): Promise<any> {
  //   const config = getConfiguration();
  //   const options = getOption(METHOD.GET);
  //   const res = await fetch(config.apiEndpoint + url, options);
  //   const data = await res.blob();
  //   return data;
  // }
  // post
  // postForm
  // async postForm(url: string, body: any): Promise<ResponseModel<T>> {
  //   const config = getConfiguration();
  //   const options = getOption(METHOD.POST, body, true);
  //   const res = await fetch(config.apiEndpoint + url, options);
  //   const data: ResponseModel<T> = await res.json();
  //   return data;
  // }
  // putForm
  // async putForm(url, body) {
  // 	const options = {
  // 		method: 'PUT',
  // 		body
  // 	};
  // 	const res = await fetch(this.endpoint + url, options);
  // 	const data = await res.json();
  // 	return data;
  // }
  // delete
  // async delete(url: string, body: any): Promise<ResponseModel<T>> {
  //   const config = getConfiguration();
  //   const options = getOption(METHOD.DELETE, body);
  //   const res = await fetch(config.apiEndpoint + url, options);
  //   const data: ResponseModel<T> = await res.json();
  //   return data;
  // }
  // deleteForm
  // async deleteForm(url:string, body:any) {
  // 	const options = {
  // 		method: 'DELETE',
  // 		body
  // 	};
  // 	const res = await fetch(this.endpoint + url, options);
  // 	const data = await res.json();
  // 	return data;
  // }
};
async function toResponse(res) {
  if (res.ok) {
    const responseBody = {
      data: await res.json(),
      status: true,
      statusCode: 200
    };
    return responseBody;
  } else {
    const errorBody = await res.json();
    const responseBody = {
      status: false,
      statusCode: res.status,
      fields: errorBody.fields,
      message: errorBody.message
    };
    return responseBody;
  }
}
async function toCatchError(err) {
  return {
    status: false,
    statusCode: 500,
    fields: void 0,
    message: err.message
  };
}

// ts/global/endpoint.ts
function getEndPoint() {
  return "http://localhost:3000/";
}

// ts/authentication/authenticationService.ts
async function SendVerificationCode(model) {
  const fetcher = new Fetcher(getEndPoint());
  const response = await fetcher.post("auth/SendVerificationCode", model);
  return response;
}
async function SignIn(model) {
  const fetcher = new Fetcher(getEndPoint());
  const response = await fetcher.post("auth/SignIn", model);
  return response;
}
async function CompleteRegister(model) {
  const fetcher = new Fetcher(getEndPoint());
  const response = await fetcher.post("auth/CompleteRegister", model);
  return response;
}

// ts/static/locationService.ts
async function GetCountryList() {
  const fetcher = new Fetcher(getEndPoint());
  const response = await fetcher.get("static/GetCountryList");
  return response;
}
async function GetProvinceList(requestId) {
  const fetcher = new Fetcher(getEndPoint());
  const response = await fetcher.get("static/GetProvinceList/" + requestId);
  return response;
}
async function GetCityList(requestId) {
  const fetcher = new Fetcher(getEndPoint());
  const response = await fetcher.get("static/GetCityList/" + requestId);
  return response;
}

// ts/team/postService.ts
async function GetPostsByTeamId(requestId) {
  const fetcher = new Fetcher(getEndPoint());
  const response = await fetcher.get("team/post/list/" + requestId);
  return response;
}
async function CreatePost(model) {
  const fetcher = new Fetcher(getEndPoint());
  const response = await fetcher.post("team/post", model);
  return response;
}

// ts/team/teamCategoryService.ts
async function GetTeamCategory() {
  const fetcher = new Fetcher(getEndPoint());
  const response = await fetcher.get("team/category");
  return response;
}

// ts/team/teamMemberRequestService.ts
async function GetOwnTeamMemberRequest() {
  const fetcher = new Fetcher(getEndPoint());
  const response = await fetcher.get("team/request/getOwnTeamMemberRequest");
  return response;
}
async function GetTeamMemberRequestList(requestId) {
  const fetcher = new Fetcher(getEndPoint());
  const response = await fetcher.get("team/request/getByTeamId/" + requestId);
  return response;
}
async function RequestForJoinToTeam(model) {
  const fetcher = new Fetcher(getEndPoint());
  const response = await fetcher.post("team/request/requestForJoinToTeam", model);
  return response;
}
async function RejectJoinRequest(requestId) {
  const fetcher = new Fetcher(getEndPoint());
  const response = await fetcher.get("team/request/rejectJoinRequest/" + requestId);
  return response;
}

// ts/team/teamService.ts
async function CreateTeam(model) {
  const fetcher = new Fetcher(getEndPoint());
  const response = await fetcher.post("team/createTeam", model);
  return response;
}
async function ListTeam() {
  const fetcher = new Fetcher(getEndPoint());
  const response = await fetcher.get("team/listTeam");
  return response;
}
async function GetMyTeams(requestId) {
  const fetcher = new Fetcher(getEndPoint());
  const response = await fetcher.get("team/myTeams/" + requestId);
  return response;
}
async function GetTeamById(requestId) {
  const fetcher = new Fetcher(getEndPoint());
  const response = await fetcher.get("team/" + requestId);
  return response;
}
async function UpdateTeam(model) {
  const fetcher = new Fetcher(getEndPoint());
  const response = await fetcher.put("team", model);
  return response;
}
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  CompleteRegister,
  CreatePost,
  CreateTeam,
  GetCityList,
  GetCountryList,
  GetMyTeams,
  GetOwnTeamMemberRequest,
  GetPostsByTeamId,
  GetProvinceList,
  GetTeamById,
  GetTeamCategory,
  GetTeamMemberRequestList,
  ListTeam,
  RejectJoinRequest,
  RequestForJoinToTeam,
  SendVerificationCode,
  SignIn,
  UpdateTeam
});
