import * as global from '../global';
import { OwnTeamMemberResponse, ListTeamMemberRequest, RequestForJoinToTeamRequest, RequestForJoinToTeamResponse } from './teamMemberRequest';
export declare function GetOwnTeamMemberRequest(): Promise<global.ResponseModel<OwnTeamMemberResponse>>;
export declare function GetTeamMemberRequestList(requestId?: string): Promise<global.ResponseModel<ListTeamMemberRequest>>;
export declare function RequestForJoinToTeam(model: RequestForJoinToTeamRequest): Promise<global.ResponseModel<RequestForJoinToTeamResponse>>;
export declare function RejectJoinRequest(requestId?: string): Promise<global.ResponseModel<string>>;
