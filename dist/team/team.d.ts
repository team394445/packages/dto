export interface Team {
    id?: string;
    name: string;
    title: string;
    isPublic: boolean;
    description: string;
    countryId?: string;
    provinceId?: string;
    cityId?: string;
    categoryId?: string;
}
export interface ListTeamResponse {
    teams: Array<Team>;
}
export interface GetTeamByIdRequest {
    teamId: string;
}
export interface GetTeamByIdResponse {
    team: Team;
}
export interface CreateTeamRequest {
    team: Team;
}
export interface CreateTeamResponse {
    teamId: string;
}
export interface UpdateTeamRequest {
    team: Team;
}
export interface UpdateTeamResponse {
    team: Team;
}
