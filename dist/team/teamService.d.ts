import * as global from '../global';
import { ListTeamResponse, GetTeamByIdResponse, CreateTeamRequest, CreateTeamResponse, UpdateTeamRequest } from './team';
export declare function CreateTeam(model: CreateTeamRequest): Promise<global.ResponseModel<CreateTeamResponse>>;
export declare function ListTeam(): Promise<global.ResponseModel<ListTeamResponse>>;
export declare function GetMyTeams(requestId?: string): Promise<global.ResponseModel<ListTeamResponse>>;
export declare function GetTeamById(requestId?: string): Promise<global.ResponseModel<GetTeamByIdResponse>>;
export declare function UpdateTeam(model: UpdateTeamRequest): Promise<global.ResponseModel<boolean>>;
