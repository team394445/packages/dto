export interface Post {
    id: string;
    teamId: string;
    title: string;
    description: string;
    isPublic: boolean;
    likeCount: number;
    dislikeCount: number;
    viewCount: number;
    allowComment: boolean;
    isPublished: boolean;
    creatorId: string;
    createAt: number;
    updateAt: number;
}
export interface GetPostsByTeamIdResponse {
    postList: Array<Post>;
}
export interface CreatePostRequest {
    post: Post;
}
export interface CreatePostResponse {
    postId: string;
}
