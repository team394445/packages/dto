export interface AddMemberToTeamRequest {
    teamId: string;
    userId: string;
    role: string;
}
export interface AddMemberToTeamResponse {
    teamId: string;
    userId: string;
    role: string;
}
