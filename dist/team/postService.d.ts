import * as global from '../global';
import { GetPostsByTeamIdResponse, CreatePostRequest, CreatePostResponse } from './post';
export declare function GetPostsByTeamId(requestId?: string): Promise<global.ResponseModel<GetPostsByTeamIdResponse>>;
export declare function CreatePost(model: CreatePostRequest): Promise<global.ResponseModel<CreatePostResponse>>;
