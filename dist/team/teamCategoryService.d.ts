import * as global from '../global';
import { ListTeamCategoryResponse } from './teamCategory';
export declare function GetTeamCategory(): Promise<global.ResponseModel<ListTeamCategoryResponse>>;
