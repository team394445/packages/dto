import * as global from '../global';
import { SendVerificationCodeRequest, SendVerificationCodeResponse, SignInRequest, SignInResponse, CompleteRegisterRequest, CompleteRegisterResponse } from './authentication';
export declare function SendVerificationCode(model: SendVerificationCodeRequest): Promise<global.ResponseModel<SendVerificationCodeResponse>>;
export declare function SignIn(model: SignInRequest): Promise<global.ResponseModel<SignInResponse>>;
export declare function CompleteRegister(model: CompleteRegisterRequest): Promise<global.ResponseModel<CompleteRegisterResponse>>;
