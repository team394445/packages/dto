export interface City {
    id: string;
    provinceId: string;
    name: string;
}
export interface Province {
    id: string;
    countryId: string;
    name: string;
}
export interface Country {
    id: string;
    name: string;
}
export interface GetCountryListRequest {
}
export interface GetCountryListResponse {
    countryList: Array<Country>;
}
export interface GetProvinceByCountryIdRequest {
    countryId: string;
}
export interface GetProvinceByCountryIdResponse {
    provinceList: Array<Province>;
}
export interface GetCityByProvinceIdRequest {
    provinceId: string;
}
export interface GetCityByProvinceIdResponse {
    cityList: Array<City>;
}
