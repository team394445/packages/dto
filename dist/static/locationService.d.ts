import * as global from '../global';
import { GetCountryListResponse, GetProvinceByCountryIdResponse, GetCityByProvinceIdResponse } from './location';
export declare function GetCountryList(): Promise<global.ResponseModel<GetCountryListResponse>>;
export declare function GetProvinceList(requestId?: string): Promise<global.ResponseModel<GetProvinceByCountryIdResponse>>;
export declare function GetCityList(requestId?: string): Promise<global.ResponseModel<GetCityByProvinceIdResponse>>;
